﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionProject.Models
{
    public class Author
    {
        [Key]
        public int AuthorId { get; set; }

        [Required, StringLength(255), Display(Name="Author Name")]
        public string AuthorName { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}