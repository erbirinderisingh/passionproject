﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PassionProject.Models;

namespace PassionProject.ViewModels
{
    public class NewBookViewModel
    {
        public IEnumerable<Author> AuthorsName { get; set; }
        public IEnumerable<Genre> GenreNames { get; set; }
        public virtual Book Book { get; set; }
    }
}