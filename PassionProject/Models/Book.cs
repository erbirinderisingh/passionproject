﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionProject.Models
{
    public class Book
    {   
        [Key]
        public int BookId { get; set; }

        [Required, StringLength(255), Display(Name ="Book Title")]
        public string Title { get; set; }

        [StringLength(2000), Display(Name = "Book Description")]
        public string Description { get; set; }
        
        public virtual Author author { get; set; }
        public virtual Genre genre { get; set; }

    }
}