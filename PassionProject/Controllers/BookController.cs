﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Net;
using PassionProject.ViewModels;



namespace PassionProject.Controllers
{
    public class BookController : Controller
    {
        private ApplicationDBContext db = new ApplicationDBContext();

        public ActionResult Index()
        {
            return View(db.Books.ToList());
        }

        public ActionResult Create()
        {
            var viewmodel = new NewBookViewModel
            {
                AuthorsName = db.Authors.ToList(),
                GenreNames = db.Genres.ToList()
            };
            return View(viewmodel);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewBookViewModel newbook = new NewBookViewModel();
            newbook.Book = db.Books.Find(id);
            newbook.AuthorsName = db.Authors.ToList();
            newbook.GenreNames = db.Genres.ToList();
            if(newbook==null)
            {
                return HttpNotFound();
            }
            return View(newbook);
        }

        [HttpPost]
        public ActionResult Edit(string bookTitle,int bookId,int BookAuthor, int BookGenre,string bookDescription)
        {
            string query = "update Books set Title=@title,author_AuthorId=@author,genre_GenreId=@genre,Description=@description where BookId=" + bookId;
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@title", bookTitle);
            myparams[1] = new SqlParameter("@author", BookAuthor);
            myparams[2] = new SqlParameter("@genre", BookGenre);
            myparams[3] = new SqlParameter("@description", bookDescription);
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult New(string bookTitle, int BookAuthor,int BookGenre, string bookDescription)
        {
            string query = "insert into Books (Title,author_AuthorId,genre_GenreId,Description) values (@title,@author,@genre,@description)";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@title", bookTitle);
            myparams[1] = new SqlParameter("@author", BookAuthor);
            myparams[2] = new SqlParameter("@genre", BookGenre);
            myparams[3] = new SqlParameter("@description", bookDescription);
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }
    }
}