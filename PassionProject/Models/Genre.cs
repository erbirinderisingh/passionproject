﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Genre
    {
        [Key]
        public int GenreId { get; set; }

        [Required, StringLength(255), Display(Name = "Genre")]
        public string GenreName { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}